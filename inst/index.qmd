# Welcome {.unnumbered}

```{r}
#| echo: false
#| output: false

devtools::load_all()
```

This document illustrates the same Quarto [@allaire2022] document being converted to HTML and DOCX.
See @sec-vis for example with data visualisation created with `ggplot2`.

## Acknowledgements {.unnumbered}

## Conflict of Interest {.unnumbered}

## Funding Information {.unnumbered}

## Ethics Statement {.unnumbered}

## Author Contributions {.unnumbered}

## Data Availability Statement {.unnumbered}
